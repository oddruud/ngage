﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using SharpOSC;
using System;

public class Blob {
	public int blobId;
	public int persistentId;
	public int groudId;
	public float CenterPosX;
	public float CenterPosY;
	public float VelocityX;
	public float VelocityY;
	public float BoundingBoxX;
	public float BoundingBoxY;
	public float BoundingBoxWidth;
	public float BoundingBoxHeight;
	public GameObject GameObject;
}

public class NgageController : MonoBehaviour {
	public GameObject Ball;

	public float ScaleX = 8.5f;
	public float ScaleY = 5.0f;

	public string destinationId = "192.168.7.5";
	public string clientIp = "";
	public int port = 7004;

	protected UDPSender _sender;
	protected UDPListener _listener;

	protected const string ADD_CLIENT_IP = "/OSC/AddClientIP";

	protected List<Blob> _blobs = new List<Blob>();


	// Use this for initialization
	void Start () {
		Connect();
	}

	void Connect() {
		Debug.Log("Attempting to connect to " + destinationId + ":" + port);
		_sender = new SharpOSC.UDPSender(destinationId, port);
		_listener = new UDPListener(port, Handler);
		AddClientIp(clientIp);	
	}

	void AddClientIp(string aClientIp) {
		try {

			string[] ipNumbers = aClientIp.Split('.');

			OscMessage message = new OscMessage(ADD_CLIENT_IP, int.Parse(ipNumbers[0]), int.Parse(ipNumbers[1]), int.Parse(ipNumbers[2]), int.Parse(ipNumbers[3]));
			_sender.Send(message);
		} catch(Exception e) {	
			Debug.Log("exception: " + e.ToString());
		}

	}

	private void Handler(OscPacket packet) {

		//Debug.Log("packet received: " + packet);

           var messageReceived = (OscMessage)packet;
            
            switch (messageReceived.Address)
            {
                case "/ImageProcessing/Blobs/BlobData":
                    HandleBlobData(messageReceived);
                    break;
                case "/ImageProcessing/NewFrame":
                    break;
                case "/ImageProcessing/Faces/FaceData":
                    break;
                default:
                    Console.WriteLine("Unknown OSC packet with address " + messageReceived.Address);
                    break;
            }
        }

        private void HandleBlobData(OscMessage message) {
            int blobId = (int)message.Arguments[0];
            int groudId = (int)message.Arguments[1];
            float CenterPosX = (float)message.Arguments[2];
            float CenterPosY = (float)message.Arguments[3];
            float VelocityX = (float)message.Arguments[4];
            float VelocityY = (float)message.Arguments[5];
            float BoundingBoxX = (float)message.Arguments[6];
            float BoundingBoxY = (float)message.Arguments[7];
            float BoundingBoxWidth = (float)message.Arguments[8];
            float BoundingBoxHeight = (float)message.Arguments[9];

            //check if blob with id is in list:
            Blob blob = _blobs.Find(b => b.blobId == blobId);
            if (blob == null) {
            	//create new blob
            	blob = new Blob();
				blob.blobId = blobId;
				_blobs.Add(blob);

				//

            }

			blob.blobId = blobId;
			blob.persistentId = blobId;
			blob.groudId = groudId;
			blob.CenterPosX = (CenterPosX - 0.5f) * 2f;
			blob.CenterPosY = (CenterPosY- 0.5f) * 2f;
			blob.VelocityX = VelocityX; 
			blob.VelocityY = VelocityY;
			blob.BoundingBoxX = BoundingBoxX;
			blob.BoundingBoxY = BoundingBoxY;
			blob.BoundingBoxWidth = BoundingBoxWidth;
			blob.BoundingBoxHeight = BoundingBoxHeight;
        }

        void Update() {
        	foreach(Blob b in _blobs) {

        		if (b.GameObject == null) {
					b.GameObject = GameObject.Instantiate(Ball);
					b.GameObject.name = "blob" + b.blobId.ToString();

					Debug.Log("blob " + b.blobId + " at X: " + b.CenterPosX + ",Y:" + b.CenterPosY);
				}
			
				b.GameObject.transform.position = new Vector3(b.CenterPosX * ScaleX, b.CenterPosY * ScaleY, 0f);
        	}
        }
}
